class MinStack {
  stack: number[];

  constructor() {
    this.stack = [];
  }

  push(val: number) {
    this.stack.push(val);
  }

  pop() {
    if (this.stack.length === 0) {
      return 'Underflow';
    }
    return this.stack.pop();
  }

  top() {
    return this.stack[this.stack.length - 1];
  }

  getMin() {
    if (this.stack.length === 1) {
      return this.stack[0];
    }

    let min = this.stack[0];
    for (let i = 0; i < this.stack.length; ++i) {
      if (min > this.stack[i]) {
        min = this.stack[i];
      }
    }

    return min;
  }

  isEmpty() {
    return this.stack.length === 0;
  }

  printStack() {
    let result = '';

    for (let i = 0; i < this.stack.length; ++i) {
      result += this.stack[i] + ' ';
    }

    return result;
  }
}

function match(left: string, right: string) {
  switch (left) {
    case '(':
      if (right === ')') return true;
    case '{':
      if (right === '}') return true;
    case '[':
      if (right === ']') return true;
  }
  return false;
}

function validParentheses(str: string) {
  let arr: any[];
  arr = [];

  if (str[0] === ')' || str[0] === '}' || str[0] === ']') {
    return false;
  }

  for (let i = 0; i < str.length; ++i) {
    if (str[i] === '(' || str[i] === '{' || str[i] === '[') {
      arr.push(str[i]);
    } else {
      if (!match(arr[arr.length - 1], str[i])) {
        return false;
      } else {
        arr.pop();
      }
    }
  }
  return true;
}
export  {MinStack};
export  {validParentheses};
