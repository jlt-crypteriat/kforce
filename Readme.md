# Assignment

Please find working code for the assignment.  Since the first exercise seemed to indicate that it should use a class, traditional object-oriented TypeScript was used.

### Quick Start

```
npm i
npm test
```
