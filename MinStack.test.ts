import {MinStack,  validParentheses } from './MinStack';

describe('MinStack class', () => {
  const minStack = new MinStack();
  it('MinStack class method initializes', () => {
    minStack.printStack();
  })
  it('runs/tests MinStack class', () => {
    minStack.push(-3);
    minStack.push(-1);
    minStack.push(0);
    minStack.push(23);
    console.log('Test push:', minStack.printStack());
    minStack.pop();
    console.log('Test pop:', minStack.printStack());

    console.log('Test top:', minStack.top());

    let minStack1 = new MinStack();
    minStack1.push(56);
    console.log('\nStack is:', minStack1.printStack());
    console.log('Test getMin:', minStack1.getMin());

    minStack.push(48);
    console.log('\nStack is:', minStack.printStack());
    console.log('Test getMin:', minStack.getMin());
  })
})

describe('Validate Parentheses', () => {
  it('testing validating matching pairs', () => {
    validParentheses('()');
    validParentheses(')[]');
    validParentheses('()[]{}');
    validParentheses('([)]');
    validParentheses('{[]}');
  })
})
